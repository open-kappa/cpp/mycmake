MyCMake modules reference
=========================
Core modules
^^^^^^^^^^^^

These are the modules providing the MyCMake core functionalities.
They must be used with :command:`find_package` command.

.. toctree::
    :maxdepth: 1

    /module/FindMyCMakeBase
    /module/FindMyCMakeCompiler
    /module/FindMyCMakeCTest
    /module/FindMyCMakeTargets


Utility modules
^^^^^^^^^^^^^^^

These modules are loaded using the :command:`include` command.

.. toctree::
    :maxdepth: 1

    /module/CheckCLinkerFlag
    /module/CheckCXXLinkerFlag

Find modules
^^^^^^^^^^^^

These modules must be used with :command:`find_package` command.

.. toctree::
    :maxdepth: 1

    /module/FindMyCMakeDoxygen
    /module/FindMyCMakeSphinx
    /module/FindMyCMakeSystemC

Internal modules
^^^^^^^^^^^^^^^^

These modules are MyCMake internal modules, therefore should not be
used directly by client scripts.
Their public functionalities can be used by including the public modules.

.. toctree::
    :maxdepth: 1

    /module/FindMyCMakeClang
    /module/FindMyCMakeGcc
    /module/FindMyCMakeGlobals
    /module/FindMyCMakeLicense
    /module/FindMyCMakeMessage
    /module/FindMyCMakeMSVC
    /module/FindMyCMakePolicy
    /module/FindMyCMakeSupportVariables
