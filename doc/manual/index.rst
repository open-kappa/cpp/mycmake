.. MyCMake documentation master file, created by
   sphinx-quickstart on Tue Apr 16 17:59:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:gitlab_url: https://gitlab.com/open-kappa/cpp/mycmake

MyCMake
=======

**Welcome to MyCMake documentation!**

MyCMake is a set of CMake scripts which extends the default capabilities of
CMake, in two ways:

* Adding features not directly provided by CMake at the current moment
* Adding useful defaults, code conventions, etc. to simplify prject management

As such, the defaults and code conventions are handful w.r.t. the scripts author
usual workflow: MyCMake users could not find them useful, and, in this case,
they are invited to customize these scripts.

MyCmake is free software, release under the MIT license.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   license
   requirements
   quick_start
   modules
   contributing


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
