License and contributors
------------------------

License
^^^^^^^

.. include:: ../LICENSE.txt

Contributors
^^^^^^^^^^^^

.. include:: ../CONTRIBUTORS.txt
