MyCMake requirements
--------------------

Using MyCMake scripts
^^^^^^^^^^^^^^^^^^^^^

To use MyCMake scripts, there are no special requirements.

There will be some usage lmitations. For example, they are not currently tested
under MacOS X, only few compilers are supported, etc.
If you incour in some of these limitations, fell free to contribute!

Contributing to MyCMake scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To contribute to MyCMake scripts, please make sure to have:

* Whatever required to run the regression tests (at least a compiler)
* A recent Python installation, with Sphinx installed
* The Python sphinxcontrib-moderncmakedomain module
* The Python sphinx_rtd_theme module

