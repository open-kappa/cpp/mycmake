MyCMake - A set of CMake extension scripts
Copyright 2019 Francesco Stefanni
https://gitlab.com/open-kappa/cpp/mycmake

MyCMake is distributed under the MIT license.
Please see the LICENSE.txt file inside the doc directory
to read the full license.

------------------------------------------------------------------------------

For a list of contributors, please see the CONTRIBUTORS.txt file.
