# The platform name, e.g. esp32, Heltec
set(ARDUINO_name)
# The version
set(ARDUINO_version)

# The path to the temporary folder to store build artifacts
set(ARDUINO_build.path)
# The project name
set(ARDUINO_build.project_name)
# The MCU architecture (avr, sam, etc...)
set(ARDUINO_build.arch)

# The list of include paths in the format "-I/include/path -I/another/path...."
set(ARDUINO_includes)
# The path to the source file
set(ARDUINO_source_file)
# The path to the output file
set(ARDUINO_object_file)

# Receipe for C files
set(ARDUINO_recipe.c.o.pattern)
# Receipe for CPP files
set(ARDUINO_recipe.cpp.o.pattern)
# Receipe for Assembly files
set(ARDUINO_recipe.S.o.pattern)


# The object file to include in the archive
set(ARDUINO_object_file)
# Fully qualified archive file (ex. "/path/to/core.a"). This property was added
# in Arduino IDE 1.6.6/arduino builder 1.0.0-beta12 as a replacement
# for {build.path}/{archive_file}.
set(ARDUINO_archive_file_path)
# The name of the resulting archive (ex. "core.a")
set(ARDUINO_archive_file)
# Static lib for the core
set(ARDUINO_recipe.ar.pattern)

# The list of object files to include in the archive
set(ARDUINO_object_files)
# Fully qualified archive file (ex. "/path/to/core.a"). This property was added
# in Arduino IDE 1.6.6/arduino builder 1.0.0-beta12 as a replacement
# for {build.path}/{archive_file}
set(archive_file_path)
# The linking flags for precompiled libraries
set(ARDUINO_compiler.libraries.ldflags)
# The name of the core archive file
set(ARDUINO_archive_file)
# All the artifacts produced by the previous steps (sketch object files,
# libraries object files and core.a archive) are linked together using
# the recipe.c.combine.pattern
set(ARDUINO_recipe.c.combine.pattern)
