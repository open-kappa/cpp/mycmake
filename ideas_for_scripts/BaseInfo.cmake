
if ("${CMAKE_HOST_SYSTEM_NAME}" STREQUAL "Linux")
    set(ARDUINO_OVERRIDE_SUFFIX ".linux")
elseif ("${CMAKE_HOST_SYSTEM_NAME}" STREQUAL "Windows")
set(ARDUINO_OVERRIDE_SUFFIX ".windows")
elseif ("${CMAKE_HOST_SYSTEM_NAME}" STREQUAL "Darwin")
    set(ARDUINO_OVERRIDE_SUFFIX ".macosx")
else()
    message(WARNING "Unable to detect the specialized suffix")
    set(ARDUINO_OVERRIDE_SUFFIX "")
endif()

# The absolute path of the board platform folder (i.e. the folder containing
# boards.txt)
set(ARDUINO_runtime.platform.path "")
# The absolute path of the hardware folder (i.e. the folder containing the board
# platform folder)
set(ARDUINO_runtime.hardware.path "")
# The absolute path of the Arduino IDE or Arduino CLI folder
set(ARDUINO_runtime.ide.path "")
# The version number of the Arduino IDE as a number (this uses two digits per
# version number component, and removes the points and leading zeroes, so
# Arduino IDE 1.8.3 becomes 01.08.03 which becomes runtime.ide.version=10803).
set(ARDUINO_runtime.ide.version "")
# Compatibility alias for {runtime.ide.version}
set(ARDUINO_ide_version "")
# The running OS ("linux", "windows", "macosx")
set(ARDUINO_runtime.os "")
# Set to "ARDUINO"
set(ARDUINO_software "ARDUINO")
# Platform vendor name
set(ARDUINO_name)
# Board ID of the board being compiled for (i.e. the third piece of FQBN)
set(ARDUINO_id)
# The FQBN (fully qualified board name)
set(ARDUINO_build.fqbn )
# Path to the sketch being compiled
set(ARDUINO_build.source.path )
# Set to 1 during library discovery and to 0 during normal build
set(ARDUINO_build.library_discovery_phase)
# See "Sketch debugging configuration" for details
set(ARDUINO_compiler.optimization_flags)
# Unix time (seconds since 1970-01-01T00:00:00Z) according to the machine the
# build is running on
set(ARDUINO_extra.time.utc)
# Unix time with local timezone and DST offset
set(ARDUINO_extra.time.local)
# Local timezone offset without the DST component
set(ARDUINO_extra.time.zone)
# Local daylight savings time offset
set(ARDUINO_extra.time.dst)
