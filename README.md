# MYCMAKE

MyCMake is a set of CMake scripts which extends the default capabilities of
CMake, in two ways:

* Adding features not directly provided by CMake at the current moment
* Adding useful defaults, code conventions, etc. to simplify prject management

As such, the defaults and code conventions are handful w.r.t. the scripts author
usual workflow: MyCMake users could not find them useful, and, in this case,
they are invited to customize these scripts.

## License
```
MyCMake

© 2019 Francesco Stefanni.
https://gitlab.com/open-kappa/cpp/mycmake

MyCMake is distributed under the MIT license.
Please see the license file or execute the "mycmake-license"
target to display the license.
```

## Documentation

Please see the official MyCMake documentation for quick-starts, guides,
references, etc.

https://open-kappa.gitlab.io/cpp/mycmake
