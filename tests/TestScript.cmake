# Perform the cmake test by running:
# - Delete on possible old files
# - CMake configure
# - Compiler
# - Executing
# - Checking existance of installed files

cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)

set(CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    ${MYCMAKE_SCRIPTS_DIR}
)

find_package(MyCMakeGlobals)
find_package(MyCMakeSupportVariables)
find_package(MyCMakeBase)
mycmake_include(GNUInstallDirs)

set(TEST_SOURCE_DIR "${MYCMAKE_CTEST_TEST_SOURCE_DIR}/${MYCMAKE_DEFAULT_REPO_DIRECTORY}")
set(TEST_BINARY_DIR "${MYCMAKE_CTEST_TEST_BINARY_DIR}/${MYCMAKE_DEFAULT_BUILD_DIRECTORY}")
if (IS_ABSOLUTE)
    set(TEST_INSTALL_DIR "${MYCMAKE_CTEST_TEST_INSTALL_DIR}")
else (IS_ABSOLUTE)
    set(TEST_INSTALL_DIR "${TEST_BINARY_DIR}/${MYCMAKE_CTEST_TEST_INSTALL_DIR}")
endif (IS_ABSOLUTE)
include(${MYCMAKE_CTEST_TEST_SOURCE_DIR}/TestGeneratedFiles.cmake)

message("CMAKE_GENERATOR: ${CMAKE_GENERATOR}")
message("TEST_SOURCE_DIR: ${TEST_SOURCE_DIR}")
message("TEST_BINARY_DIR: ${TEST_BINARY_DIR}")
message("TEST_INSTALL_DIR: ${TEST_INSTALL_DIR}")

# - Remove old dir:
file(REMOVE_RECURSE ${TEST_BINARY_DIR})
file(MAKE_DIRECTORY ${TEST_BINARY_DIR})

execute_process(
    # - CMake configure:
    COMMAND ${CMAKE_COMMAND}
        -Wdev
        -Werror=dev
        -DCMAKE_MAKE_PROGRAM=${CMAKE_MAKE_PROGRAM}
        -DMYCMAKE_DEBUG_MESSAGES=${MYCMAKE_DEBUG_MESSAGES}
        -DCMAKE_INSTALL_PREFIX=${TEST_INSTALL_DIR}
        -DCMAKE_BUILD_TYPE=Debug
        -G "${CMAKE_GENERATOR}"
        ${TEST_SOURCE_DIR}
    WORKING_DIRECTORY ${TEST_BINARY_DIR}
    RESULT_VARIABLE ERROR_VAR
)
if (ERROR_VAR)
    message(FATAL_ERROR "Failed configuring with exit code: ${ERROR_VAR}")
endif (ERROR_VAR)

execute_process(
    # - Compiling:
    COMMAND ${CMAKE_COMMAND} --build ${TEST_BINARY_DIR} -v --target install --config Debug --clean-first
    WORKING_DIRECTORY ${TEST_BINARY_DIR}
    RESULT_VARIABLE ERROR_VAR
)
if (ERROR_VAR)
    message(FATAL_ERROR "Failed compiling with exit code: ${ERROR_VAR}")
endif (ERROR_VAR)

if (TEST_GENERATED_EXE)
    # - Run:
    execute_process(
        COMMAND ./${TEST_GENERATED_EXE}
        WORKING_DIRECTORY ${TEST_INSTALL_DIR}/${TEST_GENERATED_WORKING_DIRECTORY}
        RESULT_VARIABLE ERROR_VAR
    )
    if (ERROR_VAR)
        message(FATAL_ERROR "Failed running ${TEST_GENERATED_EXE} with exit code: ${ERROR_VAR}")
    endif (ERROR_VAR)
endif (TEST_GENERATED_EXE)

# Checking test result:
foreach(FILE ${TEST_GENERATED_FILES})
    get_filename_component(F "${FILE}" ABSOLUTE BASE_DIR ${TEST_INSTALL_DIR})
    message("Checking: ${F}")
    if (NOT (EXISTS "${F}"))
        message(FATAL_ERROR "Test ${TEST_NAME} failed: missing: ${FILE}")
    endif (NOT (EXISTS "${F}"))
endforeach(FILE ${TEST_GENERATED_FILES})

foreach(FILE ${TEST_NOT_GENERATED_FILES})
    get_filename_component(F "${FILE}" ABSOLUTE BASE_DIR ${TEST_INSTALL_DIR})
    message("Checking: ${F}")
    if (EXISTS "${F}")
        message(FATAL_ERROR "Test ${TEST_NAME} failed: generated: ${FILE}")
    endif (EXISTS "${F}")
endforeach(FILE ${TEST_NOT_GENERATED_FILES})

# EOF
