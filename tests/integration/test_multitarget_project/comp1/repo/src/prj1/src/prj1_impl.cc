#include "prj1/prj1_impl.hh"
#include <iostream>
#include <prj2.hh>

PRJ1_EXPORT
void prj1_foo()
{
    prj2_foo();
    std::cout << "Prj1\n";
}
