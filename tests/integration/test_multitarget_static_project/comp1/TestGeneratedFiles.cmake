set(STATIC_LIB1 "${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}prj1${MYCMAKE_CURRENT_POSTFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")
set(STATIC_LIB2 "${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}prj2${MYCMAKE_CURRENT_POSTFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")

set(TEST_GENERATED_FILES
    "include/prj1.hh"
    "include/prj1/prj1_impl.hh"
    "include/prj1/prj1Export.h"
    "include/prj2.hh"
    "include/prj2/prj2_impl.hh"
    "include/prj2/prj2Export.h"
    "${CMAKE_INSTALL_DATAROOTDIR}/cmake/comp1Targets.cmake"
    "${TEST_BINARY_DIR}/comp1Targets.cmake"
    "${STATIC_LIB1}"
    "${STATIC_LIB2}"
    ${SHARED_LIB1_PDB}
    ${SHARED_LIB2_PDB}
)

set(TEST_NOT_GENERATED_FILES
    "src/prj1_impl.cc"
    "src/prj2_impl.cc"
)
