set(TARGET_NAME "prj2")


mycmake_add_library(${TARGET_NAME}
    STATIC
    src/prj2_impl.cc
    )

mycmake_target_link_libraries(${TARGET_NAME})
mycmake_target_include_directories(${TARGET_NAME})

mycmake_install(${TARGET_NAME})

# EOF
