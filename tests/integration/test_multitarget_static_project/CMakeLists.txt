set(COMP1_TGT "test_multitarget_static_project_comp1")
set(EXE1_TGT "test_multitarget_static_project_exe1")

mycmake_add_additional_files(
    comp1/TestGeneratedFiles.cmake
    comp1/repo/src/prj1/include/prj1.hh
    comp1/repo/src/prj1/include/prj1/prj1_impl.hh
    comp1/repo/src/prj1/src/prj1_impl.cc
    comp1/repo/src/prj2/include/prj2.hh
    comp1/repo/src/prj2/include/prj2/prj2_impl.hh
    comp1/repo/src/prj2/src/prj2_impl.cc
    comp1/repo/src/CMakeLists.txt
    comp1/repo/CMakeLists.txt
    exe1/TestGeneratedFiles.cmake
    exe1/repo/include/exe1.hh
    exe1/repo/src/exe1.cc
    exe1/repo/CMakeLists.txt
    )

mycmake_ctest_add_test(
    NAME
        ${COMP1_TGT}
    COMMAND
        ${CMAKE_COMMAND}
        -P ${TEST_SCRIPT}
    SOURCE_DIR
        ${CMAKE_CURRENT_LIST_DIR}/comp1
    INSTALL_DIR
        comp1
)

mycmake_ctest_set_ld_library_path(TEST_LD_PATH)

mycmake_ctest_add_test(
    NAME
        ${EXE1_TGT}
    COMMAND
        ${CMAKE_COMMAND}
        -P ${TEST_SCRIPT}
    DEPENDS
        ${COMP1_TGT}
    SOURCE_DIR
        ${CMAKE_CURRENT_LIST_DIR}/exe1
    INSTALL_DIR
        exe1
    ENVIRONMENT
        "${MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR}=${TEST_LD_PATH}${MYCMAKE_OS_ENV_PATH_SEPARATOR}$ENV{${MYCMAKE_OS_SHARED_LIBRARY_ENV_VAR}}"
)

# EOF
