cmake_minimum_required(VERSION 3.10.2 FATAL_ERROR)

project(comp1
    VERSION 1.0.0.0
    LANGUAGES CXX
    )

set(CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    "${PROJECT_SOURCE_DIR}/../../../../../cmake"
    )

find_package(MyCMakeTargets REQUIRED)
find_package(MyCMakeCompiler REQUIRED)

mycmake_configure_project(
    INSTALL_PATH "${PROJECT_BINARY_DIR}/${PROJECT_NAME}"
    BUILD_TYPE "Debug"
    )

add_subdirectory(src)

mycmake_export("dev")

# EOF
