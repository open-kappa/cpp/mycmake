#ifndef TEST_EXE_PRIVATE
#define TEST_EXE_PRIVATE

#include <iostream>

void foo();

void foo()
{
    std::cout << "Function called.\n";
}

#endif
