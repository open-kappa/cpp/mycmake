#include <iostream>
#include <string>
#include "../include/test_exe.hh"
#include "test_exe_private.hh"


int main()
{
    foo();
    const char * version = MYCMAKE_PROJECT_VERSION;
    int major = MYCMAKE_PROJECT_VERSION_MAJOR;
    int minor = MYCMAKE_PROJECT_VERSION_MINOR;
    int patch = MYCMAKE_PROJECT_VERSION_PATCH;
    int tweak = MYCMAKE_PROJECT_VERSION_TWEAK;
    bool isBigEndian = MYCMAKE_OS_IS_BIGENDIAN;
    int bitwidth = MYCMAKE_OS_BITWIDTH;

    std::cout
        << "version: " << std::string(version) << std::endl
        << "major: " << major << std::endl
        << "minor: " << minor << std::endl
        << "patch: " << patch << std::endl
        << "tweak: " << tweak << std::endl
        << "isBigEndian: " << isBigEndian << std::endl
        << "bitwidth: " << bitwidth << std::endl
        << std::endl;
    return EXIT_SUCCESS;
}
