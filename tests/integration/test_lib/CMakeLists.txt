mycmake_add_additional_files(
    TestGeneratedFiles.cmake
    repo/include/test_lib.hh
    repo/include/test_lib/test_lib_file.hh
    repo/src/test_lib.cc
    repo/src/test_lib_private.hh
    repo/CMakeLists.txt
    )

mycmake_ctest_add_test(
    COMMAND
        ${CMAKE_COMMAND}
        -P ${TEST_SCRIPT}
)


# EOF
