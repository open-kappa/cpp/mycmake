#ifndef TEST_LIB_PRIVATE_HH
#define TEST_LIB_PRIVATE_HH

#include <iostream>

void foo();

void foo()
{
    std::cout << "Foo!" << std::endl;
}

#endif
