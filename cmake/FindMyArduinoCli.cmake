# MyCMake https://gitlab.com/open-kappa/cpp/mycmake
# (c) Francesco Stefanni
# Distributed under the MIT License. See accompanying files COPYRIGHT.txt
# and LICENSE.txt or https://gitlab.com/open-kappa/cpp/mycmake for details.

#[=======================================================================[.rst:
FindArduinoCli
----------------

Support methods and variables to use arduino-cli.

User defined support variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: ARDUINO_CLI_EXE_HINTS

    List of hints to find the arduino-cli executable.

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

.. cmake:variable:: ArduinoCli_FOUND

    True if the module and its dependencies has been conrrectly found.

.. cmake:variable:: ArduinoCli_VERSION

    This module version

arduino-cli related variables
^^^^^^^^^^^^^^^^^^^^^^^

.. cmake:variable:: ARDUINO_CLI_EXE

    The arduino-cli executable

.. cmake:variable:: ARDUINO_CLI_USER_DIR

    The path to the Arduino directory.

Functions and macros
^^^^^^^^^^^^^^^^^^^^

- :cmake:command:`arduino_cli_exec()`
- :cmake:command:`arduino_cli_add_board_url()`
- :cmake:command:`arduino_cli_install_lib()`
- :cmake:command:`arduino_upgrade()`
- :cmake:command:`arduino_upload()`
- :cmake:command:`arduino_set_board()`
- :cmake:command:`arduino_set_partition()`
- :cmake:command:`arduino_compile()`
- :cmake:command:`arduino_target_link_libraries()`
- :cmake:command:`arduino_target()`

#]=======================================================================]

cmake_minimum_required(VERSION 3.16.3 FATAL_ERROR)
include_guard(GLOBAL)

function(_arduino_cli_find_program)
    # Internal method to find the arcuino-cli executable
    find_program(
        ARDUINO_CLI_EXE
        NAMES "arduino-cli"
        HINTS
            ${ARDUINO_CLI_EXE_HINTS}
            "$ENV{HOME}/arduino-cli/bin"
            "$ENV{USERPROFILE}/arduino-cli/bin"
            "$ENV{USERPROFILE}/Documents/arduino-cli/bin"
            "$ENV{USERPROFILE}/Documenti/arduino-cli/bin"
        DOC "arduino-cli executable"
    )
    if (NOT ARDUINO_CLI_EXE)
        message(FATAL_ERROR "Missing required arduino-cli")
    endif()
endfunction(_arduino_cli_find_program)

function(_arduino_cli_find_config CONFIGVAR REGEX_WHAT)
    # Internal method to extract a configuration value
    # @param CONFIGVAR The output var.
    # @param REGEX_WHAT The regex to match against the config dump
    execute_process(
        COMMAND ${ARDUINO_CLI_EXE} config dump
        OUTPUT_VARIABLE OUTVAR
        RESULTS_VARIABLE RES
    )
    if (NOT (RES STREQUAL "0"))
        message(FATAL_ERROR "Error in parsing arduino-cli config: ${RES}")
    endif()
    unset(RES)
    string(REGEX MATCH "${REGEX_WHAT}" THEMATCH "${OUTVAR}")
    set(${CONFIGVAR} "${THEMATCH}" PARENT_SCOPE)
endfunction(_arduino_cli_find_config)

function(_arduino_cli_find_user_path)
    # Internal support method to find the user path (i.e. the Arduino dir)
    _arduino_cli_find_config(USERDIRENTRY "user: [^\n\r]*")
    string(REGEX REPLACE "user: ([^\n\r]*)" "\\1" USERDIR "${USERDIRENTRY}")
    if (USERDIR STREQUAL "")
        message(FATA_ERROR "Unable to get arduino-cli user directory")
    endif()
    message("arduino-cli user dir: ${USERDIR}")
    set(ARDUINO_CLI_USER_DIR "${USERDIR}" PARENT_SCOPE)
endfunction(_arduino_cli_find_user_path)

_arduino_cli_find_program()
_arduino_cli_find_user_path()

find_package(PackageHandleStandardArgs)
set(MyArduinoCli_VERSION "1.0.0")

find_package_handle_standard_args(
        MyArduinoCli
    FOUND_VAR
        MyArduinoCli_FOUND
    REQUIRED_VARS
        MyArduinoCli_VERSION
        ARDUINO_CLI_EXE
        ARDUINO_CLI_USER_DIR
    VERSION_VAR
        MyArduinoCli_VERSION
)

#[=======================================================================[.rst:
.. cmake:command:: arduino_cli_exec

    Execute the arcuino-cli program, with the given parameters

    .. code-block:: cmake

        arduino_cli_exec([ARGN])

    Parameters:

    * ``[ARGN]``: The arduino-cli parameters.
#]=======================================================================]
function (arduino_cli_exec)
    execute_process(
        COMMAND ${ARDUINO_CLI_EXE} ${ARGN}
        COMMAND_ECHO STDOUT
        RESULTS_VARIABLE RES
    )
    if (NOT (RES STREQUAL "0"))
        string(REPLACE ";" " " cliExeErr "${ARDUINO_CLI_EXE} ${ARGN}")
        string(REPLACE "\n" " " cliExeErr "${cliExeErr}")
        message(SEND_ERROR "ERROR command: ${cliExeErr}")
        message(FATAL_ERROR "RESULT: ${RES}")
    endif()
    unset(RES)
endfunction (arduino_cli_exec)

#[=======================================================================[.rst:
.. cmake:command:: arduino_cli_add_board_url

    Add the URL to install additional boards.
    Avoid to add duplicated entries.

    .. code-block:: cmake

        arduino_cli_add_board_url(<URL>)

    Parameters:

    * ``<URL>``: The url to the board config file to install.
#]=======================================================================]
function (arduino_cli_add_board_url URL)
    _arduino_cli_find_config(THEURL "${URL}")
    if (THEURL STREQUAL "")
        arduino_cli_exec(config add board_manager.additional_urls "${URL}")
    else()
        message("arduino-cli board url already exists: ${URL}")
    endif()
endfunction (arduino_cli_add_board_url)

#[=======================================================================[.rst:
.. cmake:command:: arduino_cli_install_lib

    Install an Arduino library.
    Support both library names and git repo urls.

    .. code-block:: cmake

        arduino_cli_install_lib(<LIB>)

    Parameters:

    * ``<LIB>``: The library to install.
#]=======================================================================]
function (arduino_cli_install_lib LIB)
    if((LIB MATCHES "^git@.*") OR (LIB MATCHES "^https:.*"))
        get_filename_component(LIB_NAME "${LIB}" NAME_WE)
        string(REPLACE " " "_" DIRNAME "${LIB_NAME}")
        if (EXISTS "${ARDUINO_CLI_USER_DIR}/libraries/${DIRNAME}")
            message("Arduino lib already installed: ${LIB}")
        else()
            message("Installig ${LIB}...")
            arduino_cli_exec(lib install --git-url ${LIB})
        endif()
    else()
        arduino_cli_exec(lib install ${LIB})
    endif()
endfunction (arduino_cli_install_lib)

#[=======================================================================[.rst:
.. cmake:command:: arduino_upgrade

    Upgrade Arduino libraries.

    .. code-block:: cmake

        arduino_upgrade()
#]=======================================================================]
function (arduino_upgrade)
    message("Performing Arduino upgrade...")
    arduino_cli_exec(update)
    arduino_cli_exec(upgrade)
    message("Performing Arduino upgrade...done")
endfunction (arduino_upgrade)

#[=======================================================================[.rst:
.. cmake:command:: arduino_upload

    Flash the sketch.

    .. code-block:: cmake

        arduino_upload(<PORT> <FQBN> <SKETCH>)

    Parameters:

    * ``<PORT>``: The port to use for upload.
    * ``<FQBN>``: The FQBN.
    * ``<SKETCH>``: The INO sketch file.
#]=======================================================================]
function (arduino_upload PORT FQBN SKETCH)
    # Option -i skipped since it is for bin files
    arduino_cli_exec(upload -p ${PORT} --fqbn ${FQBN} ${SKETCH})
endfunction (arduino_upload)

#[=======================================================================[.rst:
.. cmake:command:: arduino_set_board

    Set the board FQBN: export in the caller scope ARDUINO_FQBN and
    ARDUINO_VENDOR, ARDUINO_ARCHITECTURE, ARDUINO_BOARD_ID.

    .. code-block:: cmake

        arduino_set_board(<NAME>)

    Parameters:

    * ``<NAME>``: The arduino-cli FQBN.
#]=======================================================================]
function(arduino_set_board NAME)
    string(REGEX REPLACE "^([^:]*):([^:]*):([^:]*).*" "\\1" VENDOR "${NAME}")
    string(REGEX REPLACE "^([^:]*):([^:]*):([^:]*).*" "\\2" ARCH "${NAME}")
    string(REGEX REPLACE "^([^:]*):([^:]*):([^:]*).*" "\\3" ID "${NAME}")
    set(FQBN "${VENDOR}:${ARCH}:${ID}")

    set(ARDUINO_VENDOR "${VENDOR}" PARENT_SCOPE)
    set(ARDUINO_ARCHITECTURE "${ARCH}" PARENT_SCOPE)
    set(ARDUINO_BOARD_ID "${ID}" PARENT_SCOPE)
    set(ARDUINO_FQBN "${FQBN}" PARENT_SCOPE)
    message("ARDUINO_FQBN: ${FQBN}")
endfunction(arduino_set_board)

#[=======================================================================[.rst:
.. cmake:command:: arduino_set_partition

    Set the partition, exporting the ARDUINO_PARTITION_NAME and ARDUINO_UPLOAD_SIZE.

    .. code-block:: cmake

        arduino_set_partition(<NAME> [<SIZE>])

    Parameters:

    * ``<NAME>``: The partition name.
    * ``<SIZE>``: The total maximum size.
#]=======================================================================]
function(arduino_set_partition NAME)
    set(ARDUINO_PARTITION_NAME "${NAME}")
    if ("${ARGV1}" STREQUAL "")
        if ("${NAME}" STREQUAL "min_spiffs")
            if ("${ARDUINO_FQBN}" STREQUAL "esp32:esp32:esp32")
                set(ARDUINO_UPLOAD_SIZE "1966080")
            else()
                message(FATAL_ERROR "Unknown partition schema for ${ARDUINO_FQBN}, please specify its maximum size")
            endif()
        else()
            message(FATAL_ERROR "Unknown partition schema name, please specify its maximum size")
        endif()
    else()
        set(ARDUINO_UPLOAD_SIZE "${ARGV1}")
    endif()
    set(ARDUINO_PARTITIONS "--build-property;build.partitions=${ARDUINO_PARTITION_NAME};--build-property;upload.maximum_size=${ARDUINO_UPLOAD_SIZE}" PARENT_SCOPE)
endfunction(arduino_set_partition)

#[=======================================================================[.rst:
.. cmake:command:: arduino_compile

    Compile the sketch.
    Assume the sketch name matches CMAKE_PROJECT_NAME.

    .. code-block:: cmake

        arduino_compile([ARGN])

            Parameters:

    * ``[ARGN]``: Optional extra compile flags.
#]=======================================================================]
function(arduino_compile)
    list(JOIN ARGN " " THIS_ARGS)
    arduino_cli_exec(
        compile
            --output-dir "${PROJECT_BINARY_DIR}/${CMAKE_PROJECT_NAME}"
            --build-path "${PROJECT_BINARY_DIR}"
            -v
            --build-property "compiler.cpp.extra_flags=${CMAKE_CXX_FLAGS} ${THIS_ARGS} -std=gnu++17"
            --build-property "compiler.c.extra_flags=${CMAKE_C_FLAGS} ${THIS_ARGS} -std=gnu11"
            ${ARDUINO_PARTITIONS}
            ${ARDULNO_LIBRARIES}
            --fqbn ${ARDUINO_FQBN}
            ${CMAKE_SOURCE_DIR}/${CMAKE_PROJECT_NAME}.ino
    )
endfunction(arduino_compile)

#[=======================================================================[.rst:
.. cmake:command:: arduino_target_link_libraries

    Add paths for library search.

    .. code-block:: cmake

        arduino_target_link_libraries([ARGN])

            Parameters:

    * ``[ARGN]``: libraries paths.
#]=======================================================================]
function(arduino_target_link_libraries)
    set(LIB_LIST)
    foreach(LIB ${ARGV})
        list(APPEND LIB_LIST --library "${LIB}")
    endforeach(LIB ${ARGV})
    set(ARDULNO_LIBRARIES ${ARDUINO_LIBRARIES} ${LIB_LIST} PARENT_SCOPE)
endfunction(arduino_target_link_libraries)

#[=======================================================================[.rst:
.. cmake:command:: arduino_target

    Declare an arduino target.
    Assume the sketch name matches CMAKE_PROJECT_NAME.

    .. code-block:: cmake

        arduino_target([ARGN])

            Parameters:

    * ``[ARGN]``: Optional extra compile flags.
#]=======================================================================]
function(arduino_target)
    list(JOIN ARGN " " THIS_ARGS)
    set(CMD
        ${ARDUINO_CLI_EXE} compile
        --output-dir "${PROJECT_BINARY_DIR}/${CMAKE_PROJECT_NAME}"
        --build-path "${PROJECT_BINARY_DIR}"
        -v
        --build-property "compiler.cpp.extra_flags=${CMAKE_CXX_FLAGS} ${THIS_ARGS} -std=gnu++17"
        --build-property "compiler.c.extra_flags=${CMAKE_C_FLAGS} ${THIS_ARGS} -std=gnu11"
        ${ARDUINO_PARTITIONS}
        ${ARDULNO_LIBRARIES}
        --fqbn ${ARDUINO_FQBN}
        ${CMAKE_SOURCE_DIR}/${CMAKE_PROJECT_NAME}.ino
    )
    set(CMD_STR "${CMD}")
    string(REPLACE ";" " " CMD_STR "${CMD_STR}")
    string(REPLACE "\n" " " CMD_STR "${CMD_STR}")
    add_custom_target(
        ${CMAKE_PROJECT_NAME}
        ALL
        COMMAND ${CMAKE_COMMAND} -E echo "${CMD_STR}"
        COMMAND ${CMD}
        COMMAND_EXPAND_LISTS
        VERBATIM
    )
endfunction(arduino_target)

#[=======================================================================[.rst:
.. cmake:command:: arduino_upload_target

    Declare a target to flash the sketch.
    Assume the sketch name matches CMAKE_PROJECT_NAME.
    The target will be: `${CMAKE_PROJECT_NAME}_upload_${SUFFIX}`

    .. code-block:: cmake

        arduino_upload_target(<PORT> <SUFFIX>)

    Parameters:

    * ``<PORT>``: The port to use for upload.
    * ``<SUFFIX>``: The suffix for the taget name.
#]=======================================================================]
function (arduino_upload_target PORT SUFFIX)
    # Option -i skipped since it is for bin files
    add_custom_target(
        ${CMAKE_PROJECT_NAME}_upload_${SUFFIX}
        COMMAND ${ARDUINO_CLI_EXE}
            -p ${PORT}
            --fqbn ${ARDUINO_FQBN}
            ${CMAKE_SOURCE_DIR}/${CMAKE_PROJECT_NAME}.ino
    )
endfunction (arduino_upload_target)
